(ns plf04.core)

(defn warmup-1 ;;lineal
  [xs]
  (letfn [(f [ys]
             (if (empty? ys)
               (if (and (> (count list) 0) (<= (count list) 3))
                  "1"
                  "2"
                 )
               (if (zero? (compare (first ys) \e))
                 (f (rest ys))
                 (cons (first ys) (f (rest ys)))
               ))
             )]
    (f xs)))

(defn warmup-2 ;;cola
  [xs] 
  (letfn [(f [ys acc]
            (if (empty? ys)
              (if (and (> (count acc) 0) (<= (count acc) 3))
                ( acc)
                (map acc)
                )
              (f (rest ys)
                 (if (= (first ys) "e")
                   (conj acc (first ys))
                   acc
                 ) 
              ))
          )]
    (f xs [])))
  


 